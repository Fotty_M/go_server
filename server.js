
const express = require('express'),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  moment = require('moment');

const  data = require('./data');


const app = express();
app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
const port = 4321;
const router = express.Router();



router.get('/', function (req, res) {
  res.json({message:'Go Server API'});
}, function (error) {
  console.log(error);
});

router.get('/subscriptions', function (req, res) {
  var lastMonth = moment(data.subscription.payments.slice(-1).pop().endDate);
  var nextBillingDate = lastMonth.add(30, 'days');
  data.subscription.nextBillingDate = nextBillingDate;
  res.json(data);
});



app.use('/api', router);
app.listen(port, function () {
  console.log('API server running on http://localhost:' + port);
});
