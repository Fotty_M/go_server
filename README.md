# Tikvaa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.2.1.

## Setup 
1. `git clone` the project into your computer
2. `cd` into the project folder
3. run `npm install` to install all the dependencies or `yarn install` if you're using yarn.
4. run `npm start` or `yarn start` to start both the server-side application and the client-side application at once.
5. from your web browser and visit `http://localhost:3456/` to open the client-side application.

##Applications
1. Client-side: Angular2
2. Server-side: ExpressJS(NodeJS)

##Assumptions 
1. The assumptions are that the client is already connected to the application and they want to see information about their subscriptions,
therefore they open the subscription page and see what is going on. That is to say, how much they have paid in the past, how long have they been subscribed and when are they going to pay next.

2. Generally a subscription is a recurring payment for a certain period of time but in this case I assumed it ends only when a user cancels the subscription.
In this case then the user will cancel the subscription which will stop the recurring payment but the subscription itself will end when the 30 days of subscription will be over.

3. The Application knows when the next payment will occur by adding 30 days to the last payment.
 
4. The cancel button should normally send a request to the server to cancel the service, then it will reflect by saying:*subscription cancelled and x days remaining for the service usage* and change the button from *CancelSubscription to Subscribe*

5. The data in the backend arent' stored in a real database but in the file `data.js` then passed to the server for it to pass it then to the client.


##Further comments 
Obviously there are some other issues which I probably didn't find or I just made wrong assumpltions, or maybe I just didn't think of them.

*Software is written by humans and therefore has bugs - John Jacobs*
