import {Component, OnInit} from '@angular/core';
import {SubscriptionService} from "./subscription.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  subscriptionsDetails: Object = {};

  constructor(private subscriptionService: SubscriptionService){}

  ngOnInit(){
    this.getSubscriptionDetails();
  }

  getSubscriptionDetails(){
    return  this.subscriptionService.getSubscription().subscribe(
      (data) => {
        this.subscriptionsDetails = data;
      },
      (error) =>{
        //TODO: Maybe a nice alert will be great here.
        console.error(error);
      });
  }

  handleClick(){
    //TODO: Talk to the service and tell it to send a post request to the server to cancel subscription or subscribe
    alert('This should tell you that the subscription was canceled');
  }

}
