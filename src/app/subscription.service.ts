import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers, Request, RequestMethod} from "@angular/http";
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map';



@Injectable()
export class SubscriptionService {

  url: string = 'http://localhost:4321/api';

  constructor(private http: Http ) { }

  getSubscription():Observable<any> {
    return this.http.get(this.url+'/subscriptions').map( (res: Response) => res.json())
  }

  cancelSubscription(){
    //TODO: Send a post request to the API to change the current subscription status
  }

  startSubscription(){
    //TODO: Send a post request to the API to change the current subscription status
  }


}
