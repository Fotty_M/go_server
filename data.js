const data = {
  "subscription": {
    "active": true,
    "service": "Go Server",
    "serviceProvider": "Premium service for € 2.99/month",
    "nextBillingDate": null,
    "payments": [
      {
        "paymentId": 1,
        "amountInCents": 299,
        "startDate": "2017-03-21 00:00:00",
        "endDate": "2017-04-21 00:00:00"
      },
      {
        "paymentId": 2,
        "amountInCents": 399,
        "startDate": "2017-04-21 00:00:00",
        "endDate": "2017-05-21 00:00:00"
      },
      {
        "paymentId": 3,
        "amountInCents": 299,
        "startDate": "2017-05-21 00:00:00",
        "endDate": "2017-06-21 00:00:00"
      }
    ]
  }
};

module.exports = data;
