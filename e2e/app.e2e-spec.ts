import { TikvaaPage } from './app.po';

describe('tikvaa App', () => {
  let page: TikvaaPage;

  beforeEach(() => {
    page = new TikvaaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
